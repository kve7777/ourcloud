var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    htmlreplace = require('gulp-html-replace'),
    sourcemaps = require('gulp-sourcemaps'),
    fs = require("fs");
//
// var version = JSON.parse(fs.readFileSync("./conf.json", "utf8")).version;
//
// fs.writeFileSync("./conf.json", JSON.stringify({version : ++version}));




gulp.task("auth", function () {

    var scriptsArray = [
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js",
        // "bower_components/placeholders/dist/*.min.js",
        "bower_components/jquery-backstretch/jquery.backstretch.min.js",
        "public/assets/auth/js/*.js",
    ];


    var stylesArray = [
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "public/assets/auth/css/*.css"
    ];


    gulp.src(scriptsArray)
        // .pipe(sourcemaps.init())
        .pipe(concat("auth.js"))
        /*.pipe(uglify({ mangle: false }))*/
        // .pipe(sourcemaps.write("maps"))
        .pipe(gulp.dest("public/build/js/"));
    //
    // gulp.src("extPlugins/**/*")
    //     .pipe(gulp.dest("build/js/plugins/"));
    //
    // gulp.src("assets/images/**/*")
    //     .pipe(gulp.dest("build/images/"));

    gulp.src(["assets/fonts/**/*", "bower_components/bootstrap/fonts/**/*", "bower_components/font-awesome/fonts/**/*"])
        .pipe(gulp.dest("public/build/fonts/"));

    gulp.src(["assets/img/**/*"])
        .pipe(gulp.dest("public/build/img/"));

    gulp.src(stylesArray)
        // .pipe(sourcemaps.init())
        .pipe(concat("auth.css"))
        // .pipe(sourcemaps.write("maps"))
        .pipe(gulp.dest("public/build/css/"));

    /*gulp.src("views/!**!/!*.html")
     .pipe(gulp.dest("build/views/"))*/
});

gulp.task("app", function () {

    var scriptsArray = [
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js",
        // "bower_components/placeholders/dist/*.min.js",
        "bower_components/jquery-backstretch/jquery.backstretch.min.js",
        "public/assets/app/js/*.js",
    ];


    var stylesArray = [
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "public/assets/app/css/*.css"
    ];

    gulp.src(scriptsArray)
    // .pipe(sourcemaps.init())
        .pipe(concat("app.js"))
        /*.pipe(uglify({ mangle: false }))*/
        // .pipe(sourcemaps.write("maps"))
        .pipe(gulp.dest("public/build/js/"));
    //
    // gulp.src("extPlugins/**/*")
    //     .pipe(gulp.dest("build/js/plugins/"));
    //
    // gulp.src("assets/images/**/*")
    //     .pipe(gulp.dest("build/images/"));

    gulp.src(["assets/fonts/**/*", "bower_components/bootstrap/fonts/**/*", "bower_components/font-awesome/fonts/**/*"])
        .pipe(gulp.dest("public/build/fonts/"));

    gulp.src(["assets/img/**/*"])
        .pipe(gulp.dest("public/build/img/"));

    gulp.src(stylesArray)
    // .pipe(sourcemaps.init())
        .pipe(concat("app.css"))
        // .pipe(sourcemaps.write("maps"))
        .pipe(gulp.dest("public/build/css/"));

    /*gulp.src("views/!**!/!*.html")
     .pipe(gulp.dest("build/views/"))*/
});

gulp.task('all', ['auth', 'app']);
