<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'user_id', 'type', 'name', 'ext', 'publish_key', 'expire_date'
    ];

    const IMAGE_TYPE = 1;

    const IMAGE_PATH = 'app/public';

    public function getImageFolder()
    {
        return storage_path(self::IMAGE_PATH);
    }

    public function getFileName()
    {
        return $this->name . '.' . $this->ext;
    }

    public function getFilePath()
    {
        return $this->getImageFolder() . '/' . $this->getFileName();
    }
    public function getLink()
    {
        return route('getImage', $this->name);
    }
}
