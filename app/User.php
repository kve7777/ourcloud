<?php

namespace App;

use App\Events\SendActivationEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected static function boot()
    {
        parent::boot();

        static::created(function($user){
            $user->makeActivationToken();

            event(new SendActivationEmail($user));
        });
    }

    /**
     * Make Activation link using activation token
     */

    public function getActivationLink()
    {
        return $this->attributes['activation_token'];
    }

    /**
     * Make activation token
     */

    public function makeActivationToken()
    {
        $this->attributes['activation_token'] = md5($this->id);

        $this->update();
    }

    /**
     * Check user activation status
     */

    public function isActive()
    {
        return (bool) $this->attributes['is_active'];
    }

    /**
     * Activate user
     */

    public function activate()
    {
        $this->update([
            'is_active' => true
        ]);
    }

    public function photos()
    {
        return $this->hasMany('App\Media');
    }
}
