<?php
    use Illuminate\Support\Facades\File;

    function putToEnv($key, $value)
    {
        $envFile = app()->environmentFilePath();

        $content = File::get($envFile);

        $pattern = "/^($key)(=?)(.*)$/m";
        $replacement = "$key=$value";

        $content = preg_replace($pattern, $replacement, $content, -1, $count);

        if ($count == 0) {
            $content .= "\n$replacement";
        }

        return File::put($envFile, $content);
    }
