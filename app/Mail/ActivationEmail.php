<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;

    public $link;

    /**
     * Create a new message instance.
     * @param User $user
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->name = $user->name;
        $this->link = $user->getActivationLink();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.activation');
    }
}
