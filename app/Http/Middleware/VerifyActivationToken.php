<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyActivationToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make(['token' => $request->route()->token], [
            'token' => 'required|exists:users,activation_token|min:32|max:32',
        ]);

        if ($validator->fails()) {
            return view('');
        }

        return $next($request);
    }
}
