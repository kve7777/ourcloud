<?php

namespace App\Http\Controllers;

use App\Media;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'active']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Media::all();

        return view('app.home', compact('photos'));
    }

    public function upload(Request $request)
    {
        $photos = [];

        foreach ($request->file('file') as $file) {

            $media = new Media([
                'type' => Media::IMAGE_TYPE,
                'name' => md5(Carbon::now()->timestamp  . $file->getClientOriginalName()),
                'ext' => $file->getClientOriginalExtension(),
            ]);

            $file->move($media->getImageFolder(), $media->getFileName());

            $photos[] = $media;
        }

        if ($photos) {
            Auth::user()->photos()->saveMany($photos);
        }

        return redirect()->back();
    }

    public function getImage($name)
    {
        $media = Media::where([
            'name' => $name,
            'type' => Media::IMAGE_TYPE
        ])->first();

        $path = null;
        $defaultFilePath = public_path('default.png');

        if ($media !== null) {
            $path = $media->getFilePath();
        }

        if ($path == null || ! File::exists($path)) {
            $path = $defaultFilePath;
        }

        $image = File::get($path);

        return response()->make($image, 200, ['content-type' => 'image/jpg']);
    }
}
