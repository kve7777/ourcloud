<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ActivationController extends Controller
{
    public function index()
    {
        return view('auth.activation');
    }

    public function activate($token)
    {
        $user = User::where('activation_token', $token)->first();

        if (! $user->isActive()) {
            $user->activate();
        }

        Auth::login($user);

        return redirect()->route('home');
    }
}
