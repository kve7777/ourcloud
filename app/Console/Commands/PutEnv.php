<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PutEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'env:put {--host=} {--port=} {--username=} {--password=} {--noPassword=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('noPassword')) {
            putToEnv('DB_PASSWORD', '');
        }

        if ($host = $this->option('host')) {
            putToEnv('DB_HOST', $host);
        }

        if ($port = $this->option('port')) {
            putToEnv('DB_PORT', $port);
        }

        if ($username = $this->option('username')) {
            putToEnv('DB_USERNAME', $username);
        }

        if ($password = $this->option('password')) {
            putToEnv('DB_PASSWORD', $password);
        }
    }
}
