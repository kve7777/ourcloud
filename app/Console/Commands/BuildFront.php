<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BuildFront extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:front';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('npm install 2>&1', $output);

        $this->showNotifications($output);

        exec('bower install 2>&1', $output);

        $this->showNotifications($output);

        exec('gulp all 2>&1', $output);

        $this->showNotifications($output);
    }

    public function showNotifications($output)
    {
        foreach ($output as $item) {
            if ($item) {
                $this->info($item);
            }
        }

        $this->info("--------------------------------------------------------------------\n");
    }
}
