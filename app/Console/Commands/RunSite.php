<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run-site';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('start chrome 127.0.0.1:8000 2>&1', $output);

        if (! empty($output)) {
            $this->info('Site available on 127.0.0.1:8000');
        }

        $this->info('Press Ctrl-C to quit.');

        exec('php artisan serve');
    }
}
