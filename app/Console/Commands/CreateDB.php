<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $putNameToEnv = false;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = null;

        try {
            $db = $this->getPdo();
            $name = $this->getDatabaseName();

            $stmt = $db->prepare("CREATE DATABASE IF NOT EXISTS $name CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;");
            $result = $stmt->execute();

            if ($result) {
                $this->info("Successfully created $name database");

                if ($this->putNameToEnv) {
                    putToEnv('DB_DATABASE', $name);
                }

                exit;
            }

            $this->error("Cannot created $name database");

        } catch (\PDOException $exception) {

            if ($name == null) {
                $this->error($exception->getMessage());

                exit;
            }

            $this->error(sprintf('Failed to create %s database, %s', $name, $exception->getMessage()));
        }
    }

    /**
     * Get name of database
     *
     * @return string|null
     */

    private function getDatabaseName()
    {
        switch (true) {
            case $name = $this->option('name') :
                $this->putNameToEnv = true;
                break;
            case $name = env('DB_DATABASE') :
                break;
            default :
                $name = $this->getDatabaseNameFromCommandLine();
                $this->putNameToEnv = true;
        }

        return $name;
    }

    /**
     * Get database name form command line
     *
     * @return string
     */

    private function getDatabaseNameFromCommandLine() : string
    {
        $name = $this->ask('What is the DB name ?');

        if (! preg_match('/^[^\W_]+$/', $name)) {
            $this->error('Database name cannot contain special characters');

            $name = null;
        }

        if ($name == null) {
            return $this->getDatabaseNameFromCommandLine();
        }

        return $name;
    }

    /**
     * Get PDO
     *
     * @return \PDO
     */

    private function getPdo() : \PDO
    {
        $database = config('database');

        $default = $database['default'];

        $connection = $database['connections'][$default];

        $credentials = [
            $connection['driver'] . ':host=' . $connection['host'] . ';port=' . $connection['port'],
            $connection['username'],
            $connection['password'],
        ];

        return new \PDO(... $credentials);
    }
}
