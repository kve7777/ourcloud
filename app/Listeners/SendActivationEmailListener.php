<?php

namespace App\Listeners;

use App\Events\SendActivationEmail;
use App\Mail\ActivationEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendActivationEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendActivationEmail  $event
     * @return void
     */
    public function handle(SendActivationEmail $event)
    {
        //Mail::to($event->user->email)->send(new ActivationEmail($event->user));
    }
}
