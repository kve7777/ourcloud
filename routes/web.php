<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::group(['prefix' => 'home'], function(){
    Route::get('/', [
        'as' => 'home',
        'uses' => 'HomeController@index']
    );

    Route::post('upload', [
        'as' => 'uploadPhoto',
        'uses' => 'HomeController@upload']
    );

    Route::get('getImage/{name}', [
        'as' => 'getImage',
        'uses' => 'HomeController@getImage']
    );
});

Route::get('/activation', 'Auth\ActivationController@index')->name('activation');

Route::get('/activation/{token}', 'Auth\ActivationController@activate')->middleware('VAT')->name('activate');
