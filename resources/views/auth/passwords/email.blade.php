@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-sm-5 center">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Reset Password</h3>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{ route('password.email') }}" method="post" class="login-form">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only" for="form-username">Enter Email</label>
                            <input type="text" name="email" placeholder="Email..." class="form-username form-control" id="form-username">
                        </div>
                        <button type="submit" class="btn">Send Password Reset Link</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
