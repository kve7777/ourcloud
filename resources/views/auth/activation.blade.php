@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-sm-5 center">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>First Activate Your Profile</h3>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
