@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-sm-5 center">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Sign up now</h3>
                        <p>Fill in the form below to get instant access:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('register')}}" method="post" class="registration-form">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only" for="form-first-name">Name</label>
                            <input type="text" name="name" placeholder="Name..." class="form-first-name form-control" id="form-first-name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Email</label>
                            <input type="text" name="email" placeholder="Email..." class="form-email form-control" id="form-email">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Password</label>
                            <input type="password" name="password" placeholder="Password..." class="form-control" id="form-password">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Confirm Password</label>
                            <input type="password" name="password_confirmation" placeholder="Confirm Password..." class="form-control" id="form-password-confirmation">
                        </div>
                        <button type="submit" class="btn">Sign me up!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
