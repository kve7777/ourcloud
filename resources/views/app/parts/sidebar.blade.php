<div class="col-lg-3">
    <h1 class="my-4">Shop Name</h1>
    <div class="list-group">
        <a href="#" class="list-group-item active">My Photos</a>
        <a href="#" class="list-group-item">Our Photos</a>
    </div>
    <form action="{{ route('uploadPhoto') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-control">
            <input type="file" name="file[]" multiple>
        </div>
        <input class="form-bottom" type="submit" value="Upload Image">
    </form>
</div>
<!-- /.col-lg-3 -->