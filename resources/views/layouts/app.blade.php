<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Item - Start Bootstrap Template</title>

    <link rel="stylesheet" href="{{ url('build/css/app.css')  }}">

</head>

<body>

<!-- Navigation -->
@include('app.parts.navbar')

<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('app.parts.sidebar')

        <div class="col-lg-9">

            @yield('content')

        </div>
        <!-- /.col-lg-9 -->

    </div>

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->
<script src="{{ url('build/js/app.js') }}"></script>

</body>

</html>
