<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>YourCloud</title>
    <link rel="stylesheet" href="{{ url('build/css/auth.css')  }}">
</head>

<body>
<div class="top-content">

    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1>Welcome to OurCloud</h1>
                </div>
            </div>

            @yield('content')

        </div>
    </div>

</div>
<!-- Top content -->
<script src="{{ url('build/js/auth.js') }}"></script>

</body>

</html>